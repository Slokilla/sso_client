##Mise en place en multiclient

Etape 1 : changer le port dans bin/www (d'ailleurs je ferais un dotenv dès que je pourrais)

Etape 2 : ajouter l'adresse avec le port nouvellement attribué dans les origines autorisées du
serveur SSO (controller/allowedOrigins.js)

/!\ En raison d'un bug en cours de résolution, deux url avec un port différent mais avec le même domaine sont condidérées comme identiques


Etape 3 : npm install   

Etape 4 : npm start