const express = require('express');
const jwt = require("jsonwebtoken");
const router = express.Router();
const User = require("../models/user.js");
const {isAuth, getProfile} = require("../controller/CAS");

/**
 * Gestion de la vérification d'authorisation.
 * Deux cas : Sois l'utilisateur n'est pas encore identifié, soit il l'est déjà.
 *
 * Dans le premier cas, on délègue notre authentification au serveur CAS.
 * Dans le second cas, on vérifie la validité du token auprès du serveur CAS
 *
 * Pour détecter si un utilisateur n'est pas connecté :
 *  On regarde d'abord si il a un token en session ou dans la requête.
 *  Si il a un token dans la requête, c'est qu'il viens de s'enregistrer.
 *  Si il a un token en session, c'est qu'il s'en enregistré auparavant.
 * Dans le cas ou il n'est pas connecté, on le redirige vers la page de login de notre CAS
 * (http://localhost:3000/auth/login)
 *
 * Dans le cas ou l'utilisateur est déjà connecté,
 * @param req
 * @param res
 * @param next
 * @return {void|*|Response}
 */
const isAuthenticated = (req, res, next) => {

    const redirectURL = `${req.protocol}://${req.headers.host}${req.path}`;

    console.log(`SSO : ${req.session.SSOID}`)
    if (req.session.SSOID == null) {
        if (req.query.ssoToken != null) {
            const {ssoToken} = req.query
            console.log(`consume token => decoded token : ${ssoToken}`);
            req.session.SSOID = ssoToken;
        }

        //Si le session ID n'est toujours pas défini après avoir regardé les paramètres de l'url
        if (req.session.SSOID == null) {
            // TODO à rajouter dans le dotenv
            return res.redirect(`http://localhost:3000/auth/login/?serviceUrl=${redirectURL}`);
        }
    }

    console.log(req.session.SSOID)
    if (req.session.SSOID) { // Si l'utilisateur a déjà son ssoID on doit le vérifier (expriration, déconnexion).
        console.log(`IsAuthenticated => ID utilisateur => ${req.session.SSOID}`);
        isAuth(req.session.SSOID).then(response => {
            if (response.isAuth) {
                req.session.expired = false;
                return next();
            } else {
                req.session.SSOID = null;
                switch (response.reason) {
                    case "expired":
                        return res.send(`<p>Votre session a expiré, reconnectez-vous : ` +
                            `<a href='http://localhost:3000/auth/login/?serviceUrl=${redirectURL}'>Se reconnecter</a></p>`);
                    case "noActiveToken":
                        return res.send(`<p>Votre identifiant est invalide : ` +
                            `<a href='http://localhost:3000/auth/login/?serviceUrl=${redirectURL}'>Se reconnecter</a></p>`);
                    case "noRequest":
                        return res.send(`<p>Votre identifiant est inexistant : ` +
                            `<a href='http://localhost:3000/auth/login/?serviceUrl=${redirectURL}'>Se reconnecter</a></p>`);
                }
            }
        });
    }
}

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Client 1'});
});

router.get('/users', isAuthenticated, async (req, res, next) => {

    await getProfile(req.session.SSOID)
        .then(response => {
            console.log(response.data)
            return res.render("user", {user: response.data});
        })
        .catch(err => {
            console.log(err);
            return res.status(500).send("Une erreur est survenue lors de la communication avec le serveur CAS. Est-il bien démarré ?",)
        });
});
module.exports = router;
