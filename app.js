const createError = require('http-errors');
const express = require('express');
const path = require('path');
const session = require('cookie-session'); // Charge le middleware de sessions
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require("mongoose");

const passport = require("passport");
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');

const app = express();


//region DB Connection
mongoose.connect("mongodb+srv://poc_auth:poc_auth_secret@agtfullstack-ykj5m.mongodb.net/client1?retryWrites=true&w=majority\n",
    { useNewUrlParser: true,
      useUnifiedTopology: true })
    .then(() => console.log("Connexion à MongoDB réussie !"))
    .catch(() => console.log("Connexion à MongoDB échouée !"));
//endregion

//region Passport
app.use(passport.initialize(undefined));
app.use(passport.session(undefined));

passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  cb(null, id);
});
//endregion

// Session engine setup
app.use(session({secret: 'S3CR370FD00M'}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
//app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
