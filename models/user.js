const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const userSchema = mongoose.Schema({
    email: {type: String, required: true, unique: true},
    token: {type: String, unique: true},
}, {
    writeConcern: {
        w: "majority",
        j: true,
        wtimeout: 1000
    }
});

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model("User", userSchema);
