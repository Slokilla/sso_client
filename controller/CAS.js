require("dotenv").config();
const axios = require("axios");

exports.isAuth = (CAStoken) => {
    if(!CAStoken){
        console.log("Communication avec le CAS => Aucun token");
    }
   return new axios.get(`${process.env.CAS_ROOT}isAuth?t=${CAStoken}`)
       .then(response => response.data)
       .catch(err => Promise.reject(err))
}

/**
 *
 * @param CAStoken
 * @return {Promise<AxiosResponse<any>>}
 */
exports.getProfile = (CAStoken) => {
    if(!CAStoken){
        console.log("Communication avec le CAS => Aucun token");
    }
   return new axios.get(`${process.env.CAS_ROOT}profile?t=${CAStoken}`)
       .then(response => response)
       .catch(err => {
          console.log("somethind happened")
          Promise.reject(err)
       })
}